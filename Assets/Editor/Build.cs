﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine.SceneManagement;
using System.Linq;
using System;
using System.IO;

// Execute build via command line
// Unity.exe -quit -batchmode -logFile stdout.log -projectPath "d:\ashley sands\Amstrike Games\TESTCMD\testCMDBuild" -executeMethod Build.buildWin

public class Build
{
	
	private static string GetArg( string argName )
	{
		string[] args = Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i].Contains( argName ))
            {
                return args[i + 1];		// args are space seperated (ie. Key Value)
            }
        }
        return null;
	}
	
	private static string GetBuildName()
	{
		string buildName = GetArg( "outputBuildName" );
		string buildTarget = GetArg( "buildTarget" );
		
		if ( buildName == null )
			throw new Exception("Unable to build outputBuildName arg not set.");
		
		if ( buildTarget != null )
			buildName = buildTarget + "." + buildName;
		
		return buildName;
	}
	
	private static string GetBuildPath()
	{
		string outputPath = GetArg( "outputPath" );
		
		if ( outputPath == null )
			throw new Exception("Unable to build outputPath arg not set.");
		
		return outputPath;
	}
	
    private static void buildWin()
    {
        
        /* // this does not seam to work command line :(
        List<string> scenes = new List<string>();

        for ( int i = 0; i < SceneManager.sceneCountInBuildSettings; i++ )
            scenes.Add( SceneManager.GetSceneByBuildIndex( i ).path );  
        */

		string outputPath = GetBuildPath();
		string outputName = GetBuildName();
		
        string[] scenes = { "Assets/Scenes/SampleScene.unity" };
        string output = outputPath + outputName + ".exe"; 

        BuildPipeline.BuildPlayer( scenes, output, BuildTarget.StandaloneWindows, BuildOptions.None );

    }
		
}


