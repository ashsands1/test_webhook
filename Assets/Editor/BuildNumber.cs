﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu( fileName = "buildCounter", menuName = "ScriptableObjects/BuildCounter", order = 1 )]
public class BuildNumber : ScriptableObject
{
    public int buildId = 0;

}
