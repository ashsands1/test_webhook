﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour
{

    public float rotateSpeed = 5f;


    void Update()
    {

        Vector3 rot = transform.eulerAngles;

        rot.y += rotateSpeed * Time.deltaTime;

        transform.eulerAngles = rot;

    }
}
