
# Dont forget to run this and set with source ./{file name}
# Set up the env vars to get the unity activation file

export UNITY_USERNAME="as223343@falmouth.ac.uk";
export UNITY_PASSWORD="";

# and request the file for a manule activation
xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' ~/project/unityCI/get_activation_file.sh

## See get_activation_file.sh for following steps :)
