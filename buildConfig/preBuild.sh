#!/usr/bin/env bash
# Pre Build

set -e
#set -x

# Set the unity license
FILE=$(pwd)/Unity_v2018.x.ulf;
export UNITY_LICENSE_CONTENT=`cat<${FILE}`;
export BUILD_TARGET="StandaloneWindows";

